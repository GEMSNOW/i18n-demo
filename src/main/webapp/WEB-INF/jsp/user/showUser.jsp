<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<%@taglib prefix="mvc" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
<head>
    <title><spring:message code="show_user_page_title"/></title>
</head>
<body>
<div>
    <span>
    <a href="?locale=zh_cn"> <spring:message code="language.zh_cn"/></a>
    <a href="?locale=zh_tw"> <spring:message code="language.zh_tw"/></a>
    <a href="?locale=en_us"> <spring:message code="language.en_us"/></a>
    </span>
</div>
<h2><spring:message code="language.username"/>:<input type="text"/></h2>
<h2><spring:message code="language.password"/>:<input type="password"/></h2>
<br/>
Locale: ${pageContext.response.locale }
</body>
</html>
