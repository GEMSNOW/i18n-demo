package com.zhou.service.user;

import com.zhou.mapper.user.UserMapper;
import com.zhou.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by zx on 2018/1/16 10:36
 */
@Service
public class UserService implements IUserService {
    @Autowired
    private UserMapper userMapper;

    @Override
    public User findById(Long id) {
        return userMapper.findById(id);
    }

    @Override
    public List<User> findAll() {
        return userMapper.findAll();
    }

    @Override
    public int delete(long id) {
        return userMapper.delete(id);
    }

    @Override
    public User update(User user) {
        return userMapper.update(user);
    }

    @Override
    public User save(User user) {
        return userMapper.save(user);
    }

    @Override
    public int batchSave(List<User> userList) {
        return userMapper.batchSave(userList);
    }
}
