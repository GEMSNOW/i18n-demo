package com.zhou.service.user;

import com.zhou.model.user.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by zx on 2018/1/16 10:38
 */
public interface IUserService {
    User findById(@Param("id") Long id);

    List<User> findAll();

    int delete(@Param("id") long id);

    User update(User user);

    User save(User user);

    int batchSave(List<User> userList);
}
