package com.zhou.mapper.user;

import com.zhou.model.user.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * Created by zx on 2018/1/15 17:54
 */
public interface UserMapper {
    User findById(@Param("id") Long id);

    List<User> findAll();

    int delete(@Param("id") long id);

    User update(User user);

    User save(User user);

    int batchSave(List<User> userList);
}
