package com.zhou.model.user;

import lombok.Data;

/**
 * Created by zx on 2018/1/16 9:42
 */
@Data
public class User {
    private Long id;
    private String username;
    private String password;
    private String gender;
    private String nickname;
}
